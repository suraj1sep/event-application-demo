package com.simplemobiletools.calendar.pro.extensions

import com.simplemobiletools.calendar.pro.helpers.CHOPPED_LIST_DEFAULT_SIZE

/**
 * Created by Suraj
 */
fun MutableList<Long>.getChoppedList(chunkSize: Int = CHOPPED_LIST_DEFAULT_SIZE): ArrayList<ArrayList<Long>> {
    val parts = ArrayList<ArrayList<Long>>()
    val listSize = this.size
    var i = 0
    while (i < listSize) {
        val newList = subList(i, Math.min(listSize, i + chunkSize)).toMutableList() as ArrayList<Long>
        parts.add(newList)
        i += chunkSize
    }
    return parts
}
