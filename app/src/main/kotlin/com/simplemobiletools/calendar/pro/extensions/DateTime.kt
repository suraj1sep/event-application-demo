package com.simplemobiletools.calendar.pro.extensions

import org.joda.time.DateTime
/**
 * Created by Suraj
 */
fun DateTime.seconds() = millis / 1000L
